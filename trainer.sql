create database trainer;
use trainer;

create table usuarios(
id int  auto_increment,
nombre text,
edad int,
sexo text,
PRIMARY KEY (id)
);

insert into usuarios values(1,"Abraham Casas",21,"Masculino");
insert into usuarios values(2,"Sandra Sanches",20,"Femenino");
insert into usuarios values(3,"Katia Itzel",21,"Femenino");