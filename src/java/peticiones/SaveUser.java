package peticiones;

import Conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "SaveUser", urlPatterns = {"/SaveUser"})
public class SaveUser extends HttpServlet {

    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    String consulta = null;
    String registro = null;
    ResultSet cdr = null;

    public void init(ServletConfig config) throws ServletException {
        super.init();
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SaveUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SaveUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String edad = request.getParameter("edad");
        String sexo = request.getParameter("sexo");

        try {
            String sql = "insert into usuarios values(null,'" + nombre + "','" + edad + "','" + sexo + "')";
            sentenciaSQL.executeUpdate(sql);
            RequestDispatcher rd = null;
            rd = request.getRequestDispatcher("DBA.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("error en post" + e);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
