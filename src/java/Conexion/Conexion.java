package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Abraham
 */
public class Conexion {

    private Connection conexion = null;

    private Statement sentenciaSQL = null;

    public Connection getConexion() {
        return conexion;
    }

    public Statement getSentenciaSQL() {
        return sentenciaSQL;
    }

    public void Conectar() {
        try {
            String controlador = "com.mysql.jdbc.Driver";
            Class.forName(controlador).newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/trainer", "root", "");
            sentenciaSQL = getConexion().createStatement();
        } catch (ClassNotFoundException ex) {
            System.out.println("No se pudo cargar el controlador: " + ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println("Objeto no creado. " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println("Acceso Ilegal. " + ex.getMessage());
        } catch (SQLException ex) {
            System.out.println("Excepcion SQL : " + ex.getMessage());
        }
    }

    public void cerrar() {
        try {
            if (getSentenciaSQL() != null) {
                getSentenciaSQL().close();
            }
            if (getConexion() != null) {
                getConexion().close();
            }

        } catch (SQLException ignorada) {
        }
    }

    public static void main(String[] args) {
        Conexion c = new Conexion();
        c.Conectar();

    }

}
