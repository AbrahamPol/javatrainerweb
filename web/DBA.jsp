<%-- 
    Document   : DBA
    Created on : 25/04/2019, 11:19:11 PM
    Author     : Abraham
--%>

<%@page import="Conexion.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java Trainer</title>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </head>
    <body>

        <jsp:include page="template.html" />

        <div class="container py-4">
            <div class="card bg-light">
                <div class="card-header">  Today's date: <%= (new java.util.Date()).toLocaleString()%> </div>
                <div class="card-body">
                    <h1>MySQL</h1>
                    <p>
                        Consulta base de datos, con jsp sin peticiones HTTP(POST,GET)
                    </p><br>

                    <%
                        ResultSet cdr = null;
                        Statement sentenciaSQL = null;
                        Conexion conecta = new Conexion();
                        conecta.Conectar();
                        sentenciaSQL = conecta.getSentenciaSQL();
                        String strComando = "select * from usuarios";
                        cdr = sentenciaSQL.executeQuery(strComando);
                        int salto = 0;
                    %>


                    <table class="table">
                        <caption>List of users</caption>
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Edad</th>
                                <th scope="col">Sexo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                while (cdr.next()) {
                            %>
                            <tr>

                                <th scope="row"><%= cdr.getString("id")%></th>
                                <td><%= cdr.getString("nombre")%></td>
                                <td><%= cdr.getString("edad")%></td>
                                <td><%= cdr.getString("sexo")%></td>

                            </tr>
                            <%                            }
                            %> 
                        </tbody>
                    </table>

                    <br>

                </div>
            </div>
        </div>
    </body>
    <footer class="container">
        <hr />
        <blockquote class="blockquote text-right">
            <p class="mb-0">Creado por Abraham</p>
            <footer class="blockquote-footer">PearBit <cite title="Source Title"></cite></footer>
        </blockquote>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Bootstrap JS -->
        <script th:src="@{/js/jquery-3.2.1.min.js}"></script>
        <script th:src="@{/js/popper.min.js}"></script>
        <script th:src="@{/js/bootstrap.min.js}"></script>
        <script th:src="@{/js/jquery-ui.min.js}"></script>
    </footer>
</html>
