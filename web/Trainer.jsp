<%-- 
    Document   : Trainer
    Created on : 25/04/2019, 08:59:57 PM
    Author     : Abraham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java Trainer</title>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </head>
    <body>

        <jsp:include page="template.html" />

        <div class="container py-4">
            <div class="card bg-light">
                <div class="card-header">  Today's date: <%= (new java.util.Date()).toLocaleString()%> </div>
                <div class="card-body">
                    <h1>JSP</h1>
                    <p>
                        JSP es un acrónimo de Java Server Pages, que en castellano 
                        vendría a decir algo como Páginas de Servidor Java. Es, pues,
                        una tecnología orientada a crear páginas web con programación 
                        en Java. 
                    </p><br>
                    <p>
                        Con JSP podemos crear aplicaciones web que se ejecuten en variados 
                        servidores web, de múltiples plataformas, ya que Java es en esencia
                        un lenguaje multiplataforma. Las páginas JSP están compuestas de 
                        código HTML/XML mezclado con etiquetas especiales para programar 
                        scripts de servidor en sintaxis Java. Por tanto, las JSP podremos
                        escribirlas con nuestro editor HTML/XML habitual.     
                    </p>
                    <br>
                    <%!String pear = "PearBit", pearLowerCase = "",
                                uno = "<" + "%!" + "String pear=PearBit %" + ">" + "  ",
                                dos = "<" + "%" + "pearLowerCase=pear.toLowerCase(); %" + ">" + "  ",
                                tres = "<" + "%=" + "pearLowerCase%" + ">" + "  ";%>
                    <%pearLowerCase = pear.toLowerCase();%>

                    <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item"> Declaracion de variables globales<br><%=uno%></li>
                        <li class="list-group-item">Codificacion<br><%=dos%></li>
                        <li class="list-group-item">Impresion de variables<br><%=tres%></li>
                    </ul>
                    <br>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-left">
                            Al hacer esto nos da como resultado
                            <span class="badge badge-primary badge-pill"><%=pearLowerCase%></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
    <footer class="container">
        <hr />
        <blockquote class="blockquote text-right">
            <p class="mb-0">Creado por Abraham</p>
            <footer class="blockquote-footer">PearBit <cite title="Source Title"></cite></footer>
        </blockquote>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Bootstrap JS -->
        <script th:src="@{/js/jquery-3.2.1.min.js}"></script>
        <script th:src="@{/js/popper.min.js}"></script>
        <script th:src="@{/js/bootstrap.min.js}"></script>
        <script th:src="@{/js/jquery-ui.min.js}"></script>
    </footer>
</html>
